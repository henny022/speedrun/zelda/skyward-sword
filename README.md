# Skyward Sword
This repo contains my resources for Zelda Skyward Sword speedrunning.
Also check [the zsr page](https://www.zeldaspeedruns.com/ss) for more info about the game.

## Routes
These routes are my notes based on the zsr ones.
- [Any%](any.md)
